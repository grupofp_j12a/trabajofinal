public class Ejercicio1a
{
    // Se asume que todos los lados del poligono son valores enteros
    public static void main(String[] args)
    {
        //ESTE CAMBIO LO HICE EN EL ENTORNO DE DESARROLLO
        //01. Declaracion de Variables
        double a;
        double b;
        double c;

        double area1, area2, areaTotal;

        //02. Asignacion de Valores
        a = 90;
        b = 70;
        c = 30;

        //03. Cálculos
        area1       = (a - c) * b / 2;  // Formula del área de un triángulo
        area2       = c * b;            // Formula del área de un rectángulo
        areaTotal   = area1 + area2;

        //04. Resultado
        System.out.println("El resultado es: " + areaTotal);
    }
}